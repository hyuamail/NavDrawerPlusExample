package id.sch.smktelkom_mlg.example.navdrawerplusexample;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import id.sch.smktelkom_mlg.example.navdrawerplusexample.fragment.RVFragment;
import id.sch.smktelkom_mlg.example.navdrawerplusexample.model.Hotel;

public class RVDetailActivity extends AppCompatActivity
{
    FloatingActionButton fab;
    FloatingActionButton fab1;
    FloatingActionButton fab2;
    LinearLayout llfab1;
    LinearLayout llfab2;
    boolean isFABOpen;
    Hotel hotel;
    boolean isUndo;
    boolean isEdited;
    CollapsingToolbarLayout ctl;
    private boolean isBackDisabled;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        llfab1 = (LinearLayout) findViewById(R.id.linearLayoutFab1);
        llfab2 = (LinearLayout) findViewById(R.id.linearLayoutFab2);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                if (!isFABOpen)
                {
                    showFABMenu();
                }
                else
                {
                    closeFABMenu();
                }
            }
        });
        fab1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                doEdit();
                closeFABMenu();
            }
        });
        fab2.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                doDelete();
                closeFABMenu();
            }
        });

        ctl = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        hotel = Hotel.findById(Hotel.class, getIntent().getLongExtra(RVFragment.HOTEL, 0));
        displayData(hotel);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                onBackPressed();
            }
        });

        AppBarLayout appBar = (AppBarLayout) findViewById(R.id.app_bar);
        appBar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener()
        {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset)
            {
                if (ctl.getHeight() + verticalOffset < 2 * ViewCompat.getMinimumHeight(ctl))
                {
                    llfab1.animate().alpha(0).setDuration(300);
                    llfab2.animate().alpha(0).setDuration(300);
                }
                else
                {
                    llfab1.animate().alpha(1).setDuration(300);
                    llfab2.animate().alpha(1).setDuration(300);
                }
            }
        });
    }

    private void displayData(Hotel hotel)
    {
        ctl.setTitle(hotel.judul);
        ImageView ivFoto = (ImageView) findViewById(R.id.imageFoto);
        ivFoto.setImageURI(Uri.parse(hotel.foto));
        TextView tvDeskripsi = (TextView) findViewById(R.id.place_detail);
        tvDeskripsi.setText(hotel.deskripsi + "\n\n" + hotel.detail);
        TextView tvLokasi = (TextView) findViewById(R.id.place_location);
        tvLokasi.setText(hotel.lokasi);
    }

    private void doDelete()
    {
        isUndo = false;
        isBackDisabled = true;
        Snackbar.make(fab, hotel.judul + " Terhapus", Snackbar.LENGTH_LONG)
                .setAction("UNDO", new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v)
                    {
                        isUndo = true;
                        isBackDisabled = false;
                    }
                })
                .setCallback(new Snackbar.Callback()
                {
                    @Override
                    public void onDismissed(Snackbar snackbar, int event)
                    {
                        if (!isUndo)
                        {
                            super.onDismissed(snackbar, event);
                            hotel.delete();
                            setResult(RESULT_OK);
                            finish();
                            Log.d("FLOW", "onDismissed: ");
                        }
                    }
                }).show();
    }

    private void doEdit()
    {
        Intent intent = new Intent(this, RVInputActivity.class);
        intent.putExtra(RVFragment.HOTEL, hotel.getId());
        startActivityForResult(intent, RVFragment.REQUEST_CODE_EDIT);
    }

    private void showFABMenu()
    {
        fab.setImageResource(R.drawable.ic_clear_black_24dp);
        llfab1.setVisibility(View.VISIBLE);
        llfab2.setVisibility(View.VISIBLE);
        llfab1.animate().translationY(getResources().getDimension(R.dimen.standard_55));
        llfab2.animate().translationY(getResources().getDimension(R.dimen.standard_105));
        isFABOpen = true;
    }

    private void closeFABMenu()
    {
        fab.setImageResource(R.drawable.ic_mode_edit_black_24dp);
        llfab1.setVisibility(View.GONE);
        llfab2.setVisibility(View.GONE);
        llfab1.animate().translationY(0);
        llfab2.animate().translationY(0);
        isFABOpen = false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RVFragment.REQUEST_CODE_EDIT && resultCode == RESULT_OK)
        {
            Log.d("FLOW", "onActivityResult: 2");
            hotel = Hotel.findById(Hotel.class, getIntent().getLongExtra(RVFragment.HOTEL, 0));
            displayData(hotel);
        }
    }

    @Override
    public void onBackPressed()
    {
        if (!isBackDisabled)
            super.onBackPressed();
        Log.d("FLOW", "onBackPressed: ");
    }
}
