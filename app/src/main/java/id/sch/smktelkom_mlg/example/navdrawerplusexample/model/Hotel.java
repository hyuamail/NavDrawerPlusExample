package id.sch.smktelkom_mlg.example.navdrawerplusexample.model;

import com.orm.SugarRecord;

/**
 * Created by hyuam on 12/10/2016.
 */

public class Hotel extends SugarRecord
{
    public String judul;
    public String deskripsi;
    public String detail;
    public String lokasi;
    public String foto;

    public Hotel()
    {
    }

    public Hotel(String judul, String deskripsi, String detail, String lokasi,
                 String foto)
    {
        this.judul = judul;
        this.deskripsi = deskripsi;
        this.detail = detail;
        this.lokasi = lokasi;
        this.foto = foto;
    }

}
