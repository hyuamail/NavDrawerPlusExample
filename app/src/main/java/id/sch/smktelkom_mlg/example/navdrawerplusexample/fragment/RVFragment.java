package id.sch.smktelkom_mlg.example.navdrawerplusexample.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.ArrayList;
import java.util.List;

import id.sch.smktelkom_mlg.example.navdrawerplusexample.R;
import id.sch.smktelkom_mlg.example.navdrawerplusexample.RVDetailActivity;
import id.sch.smktelkom_mlg.example.navdrawerplusexample.RVInputActivity;
import id.sch.smktelkom_mlg.example.navdrawerplusexample.adapter.HotelAdapter;
import id.sch.smktelkom_mlg.example.navdrawerplusexample.model.Hotel;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link //RVFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RVFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RVFragment extends Fragment implements HotelAdapter.IHotelAdapter
{
    public static final int REQUEST_CODE_ADD = 88;
    public static final int REQUEST_CODE_EDIT = 99;

    public static final String HOTEL = "hotel";
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    //    private static final String ARG_PARAM1 = "param1";
    //    private static final String ARG_PARAM2 = "param2";
    ArrayList<Hotel> mList = new ArrayList<>();
    HotelAdapter mAdapter;
    //    // TODO: Rename and change types of parameters
    //    private String mParam1;
    //    private String mParam2;
    //    private OnFragmentInteractionListener mListener;

    FloatingActionButton fab;
    RecyclerView recyclerView;
    int mItemPos;

    public RVFragment()
    {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     //     * @param param1 Parameter 1.
     //     * @param param2 Parameter 2.
     * @return A new instance of fragment RVFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RVFragment newInstance(/*String param1, String param2*/)
    {
        RVFragment fragment = new RVFragment();
        //        Bundle args = new Bundle();
        //        args.putString(ARG_PARAM1, param1);
        //        args.putString(ARG_PARAM2, param2);
        //        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        inflater.inflate(R.menu.menu_rv, menu);
        super.onCreateOptionsMenu(menu, inflater);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView)
                MenuItemCompat.getActionView(searchItem);

        MenuItemCompat.setOnActionExpandListener(searchItem,
                new MenuItemCompat.OnActionExpandListener()
        {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item)
            {
                fab.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item)
            {
                fab.setVisibility(View.VISIBLE);
                return true;
            }
        });

        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener()
                {
                    @Override
                    public boolean onQueryTextSubmit(String query)
                    {
                        return false;
                    }

                    @Override
                    public boolean onQueryTextChange(String newText)
                    {
                        String text = newText.toLowerCase();
                        doFilter(text);
                        return true;
                    }
                });
    }

    private void doFilter(String text)
    {
        if (text == null || text.isEmpty())
        {
            refreshDataAll();
        }
        else
        {
            refreshData(Select.from(Hotel.class)
                    .where(Condition.prop("judul").like("%" + text + "%"))
                    .or(Condition.prop("deskripsi").like("%" + text + "%"))
                    .or(Condition.prop("detail").like("%" + text + "%"))
                    .or(Condition.prop("lokasi").like("%" + text + "%"))
                    .list());
        }
    }

    private void refreshDataAll()
    {
        refreshData(Hotel.listAll(Hotel.class));
    }

    private void refreshData(List<Hotel> list)
    {
        mList.clear();
        mList.addAll(list);
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState)
    {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_rv, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        recyclerView = (RecyclerView) getView().findViewById(R.id.recyclerView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        mAdapter = new HotelAdapter(this, mList);
        recyclerView.setAdapter(mAdapter);

        fillData();
        //Log.d("FLOW", "onActivityCreated: F");

        fab = (FloatingActionButton) getView().findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                goAdd();
            }
        });
    }

    private void fillData()
    {
        mList.addAll(Hotel.listAll(Hotel.class));
        mAdapter.notifyDataSetChanged();
    }

    private void goAdd()
    {
        startActivityForResult(new Intent(getContext(), RVInputActivity.class), REQUEST_CODE_ADD);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_ADD && resultCode == RESULT_OK)
        {
            refreshDataAll();
            scrollToBottom();
        }
        else if (requestCode == REQUEST_CODE_EDIT)
        {
            Log.d("FLOW", "onActivityResult: <-");
            refreshDataAll();
            scrollToCurrentPos();
            Log.d("FLOW", "onActivityResult: ->");
        }
        Log.d("FLOW", "onActivityResult: ----");
    }

    private void scrollToCurrentPos()
    {
        if (mList.size() > 0)
            scrollToPos(mItemPos > mList.size() - 1 ? mList.size() - 1 : mItemPos);
    }

    private void scrollToPos(int pos)
    {
        recyclerView.scrollToPosition(pos);
    }

    private void scrollToBottom()
    {
        scrollToPos(mList.size() - 1);
    }

    //    // TODO: Rename method, update argument and hook method into UI event
    //    public void onButtonPressed(Uri uri)
    //    {
    //        if (mListener != null)
    //        {
    //            mListener.onFragmentInteraction(uri);
    //        }
    //    }

    //    @Override
    //    public void onAttach(Context context)
    //    {
    //        super.onAttach(context);
    //        if (context instanceof OnFragmentInteractionListener)
    //        {
    //            mListener = (OnFragmentInteractionListener) context;
    //        }
    //        else
    //        {
    //            throw new RuntimeException(context.toString()
    //                    + " must implement OnFragmentInteractionListener");
    //        }
    //    }
    //
    //    @Override
    //    public void onDetach()
    //    {
    //        super.onDetach();
    //        mListener = null;
    //    }
    //
    //    public interface OnFragmentInteractionListener
    //    {
    //        void showDetail(long id);
    //    }

    @Override
    public void doClick(long id, int pos)
    {
        mItemPos = pos;
        Intent intent = new Intent(getContext(), RVDetailActivity.class);
        intent.putExtra(HOTEL, id);
        startActivityForResult(intent, REQUEST_CODE_EDIT);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        Log.d("TAG", "onResume: ");
    }

    @Override
    public void onStart()
    {
        super.onStart();
        Log.d("FLOW", "onStart: ");
    }
}
